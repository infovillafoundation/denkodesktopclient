package org.bitbucket.infovillafoundation.denko.mobile;

import io.datafx.controller.flow.Flow;
import javafx.application.Application;
import javafx.stage.Stage;
import org.bitbucket.infovillafoundation.denko.mobile.controllers.MenuController;

/**
 * Created by Sandah Aung on 4/3/15.
 */

public class App extends Application {


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        new Flow(MenuController.class)
                .startInStage(primaryStage);


    }
}
