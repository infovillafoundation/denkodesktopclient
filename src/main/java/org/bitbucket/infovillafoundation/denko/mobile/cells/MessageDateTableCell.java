package org.bitbucket.infovillafoundation.denko.mobile.cells;

import javafx.scene.control.TableCell;
import org.bitbucket.infovillafoundation.denko.mobile.entities.Message;
import org.bitbucket.infovillafoundation.denko.mobile.utils.Utilities;

import java.util.Date;

/**
 * Created by Sandah Aung on 15/3/15.
 */

public class MessageDateTableCell extends TableCell<Message, Date> {
    @Override
    protected void updateItem(Date item, boolean empty) {
        super.updateItem(item, empty);
        if (!empty) {
            setText(Utilities.toDateString(item));
        } else {
            setText(null);
        }
    }
}
