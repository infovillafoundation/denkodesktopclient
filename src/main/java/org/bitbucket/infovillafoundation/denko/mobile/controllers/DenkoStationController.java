package org.bitbucket.infovillafoundation.denko.mobile.controllers;

import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.action.BackAction;
import io.datafx.controller.flow.context.ActionHandler;
import io.datafx.controller.flow.context.FlowActionHandler;
import io.datafx.controller.util.VetoException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import org.bitbucket.infovillafoundation.denko.mobile.entities.DenkoStation;
import org.bitbucket.infovillafoundation.denko.mobile.restservice.consumers.DenkoDenkoStationRestConsumer;
import org.bitbucket.infovillafoundation.denko.mobile.utils.Utilities;
import org.controlsfx.control.MasterDetailPane;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by Sandah Aung on 16/12/14.
 */

@FXMLController(value = "/fxml/denko_stations.fxml", title = "Denko Stations")
public class DenkoStationController {

    @ActionHandler
    private FlowActionHandler handler;

    @FXML
    private MasterDetailPane pane;

    @FXML
    private GridPane addDenkoStationGrid;

    @FXML
    private GridPane showDenkoStationGrid;

    @FXML
    private GridPane confirmDeleteDenkoStationGrid;

    @FXML
    private GridPane undeletableDenkoStationGrid;

    @FXML
    @BackAction
    private Button backButton;

    @FXML
    @ActionTrigger("reload")
    private Button reloadButton;

    @FXML
    @ActionTrigger("loadNewDenkoStationForm")
    private Button newDenkoStationButton;

    @FXML
    @ActionTrigger("loadEditDenkoStationForm")
    private Button editDenkoStationButton;

    @FXML
    @ActionTrigger("loadDeleteDenkoStationForm")
    private Button deleteDenkoStationButton;

    @FXML
    private TableView<DenkoStation> denkoStationTableView;

    @FXML
    @ActionTrigger("loadDenkoStationDetails")
    private MenuItem detailsMenuItem;

    @FXML
    private Text newDenkoStationText;

    @FXML
    private TextField stationNameEnglishInput;

    @FXML
    private TextField stationNameMyanmarInput;

    @FXML
    private TextField latitudeInput;

    @FXML
    private TextField longitudeInput;

    @FXML
    private TextField stationAddressEnglishInput;

    @FXML
    private TextField stationAddressMyanmarInput;

    @FXML
    @ActionTrigger("createDenkoStation")
    private Button createDenkoStationButton;

    @FXML
    @ActionTrigger("updateDenkoStation")
    private Button saveDenkoStationButton;

    @FXML
    @ActionTrigger("clearDenkoStation")
    private Button clearDenkoStationButton;

    @FXML
    private Text englishStationNameText;

    @FXML
    private Text myanmarStationNameText;

    @FXML
    private Text latitudeText;

    @FXML
    private Text longitudeText;

    @FXML
    private Text englishStationAddressText;

    @FXML
    private Text myanmarStationAddressText;

    @FXML
    @ActionTrigger("confirmDeleteDenkoStation")
    private Button confirmDeleteDenkoStationButton;

    @FXML
    @ActionTrigger("cancelDeleteDenkoStation")
    private Button cancelDeleteDenkoStationButton;

    @FXML
    @ActionTrigger("okUndeletableDenkoStation")
    private Button okUndeletableDenkoStationButton;

    @FXML
    private HBox saveEditDenkoStationButtonBar;

    @Inject
    private DenkoDenkoStationRestConsumer denkoDenkoStationRestConsumer;

    private DenkoStation currentDenkoStation;

    @PostConstruct
    public void init() {
        loadTable();
        setupBackShortcutListener();
        setupRowClickListener();
        loadNewDenkoStationForm();
        setupValidators();
    }

    private void setupRowClickListener() {
        denkoStationTableView
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(new ChangeListener<DenkoStation>() {
                                 @Override
                                 public void changed(ObservableValue<? extends DenkoStation> observable, DenkoStation oldValue, DenkoStation newValue) {
                                     currentDenkoStation = newValue;
                                     if (currentDenkoStation == null) {
                                         loadNewDenkoStationForm();
                                         editDenkoStationButton.setVisible(false);
                                         deleteDenkoStationButton.setVisible(false);
                                     } else {
                                         loadDenkoStationDetails();
                                         editDenkoStationButton.setVisible(true);
                                         deleteDenkoStationButton.setVisible(true);
                                     }

                                 }
                             }

                );
    }

    private void setupValidators() {
        setupTextFieldValidator(stationNameEnglishInput);
        setupTextFieldValidator(stationNameMyanmarInput);
        setupTextFieldValidator(latitudeInput);
        setupTextFieldValidator(longitudeInput);
        setupTextFieldValidator(stationAddressEnglishInput);
        setupTextFieldValidator(stationAddressMyanmarInput);
    }

    private void setupTextFieldValidator(TextField textField) {
        textField.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (textField.getText().isEmpty())
                    textField.setStyle("-fx-border-color: red");
                else
                    textField.setStyle("-fx-border-color: null");
            }
        });
    }

    private void setupBackShortcutListener() {
        final KeyCombination ctrlB = new KeyCodeCombination(KeyCode.B, KeyCombination.CONTROL_DOWN);
        pane.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (ctrlB.match(event)) {
                    try {
                        handler.navigateBack();
                    } catch (VetoException e) {
                    } catch (FlowException e) {
                    }
                }
            }
        });
    }

    @ActionMethod("loadDenkoStationDetails")
    public void loadDenkoStationDetails() {
        loadDenkoStationDetailsGrid();
        loadDenkoStationIntoShowGrid();
    }

    private void loadDenkoStationDetailsGrid() {
        enableThisGrid(showDenkoStationGrid);
    }

    private void enableThisGrid(GridPane grid) {
        addDenkoStationGrid.setVisible(false);
        showDenkoStationGrid.setVisible(false);
        confirmDeleteDenkoStationGrid.setVisible(false);
        undeletableDenkoStationGrid.setVisible(false);
        grid.setVisible(true);
        grid.toFront();
    }

    private void loadDenkoStationIntoShowGrid() {
        englishStationNameText.setText(currentDenkoStation.getStationNameEnglish());
        myanmarStationNameText.setText(currentDenkoStation.getStationNameMyanmar());
        latitudeText.setText(Utilities.toCurrencyFormat(currentDenkoStation.getLatitude()));
        longitudeText.setText(Utilities.toCurrencyFormat(currentDenkoStation.getLongitude()));
        englishStationAddressText.setText(currentDenkoStation.getStationAddressEnglish());
        myanmarStationAddressText.setText(currentDenkoStation.getStationAddressMyanmar());
    }

    @ActionMethod("loadNewDenkoStationForm")
    public void loadNewDenkoStationForm() {
        loadNewDenkoStationGrid();
        prepareNewDenkoStation();
    }

    private void prepareNewDenkoStation() {
        saveDenkoStationButton.setVisible(false);
        createDenkoStationButton.setVisible(true);
        createDenkoStationButton.toFront();
    }

    private void loadNewDenkoStationGrid() {
        enableThisGrid(addDenkoStationGrid);
        clearFields();
    }

    @ActionMethod("clearDenkoStation")
    public void clear() {
        clearFields();
    }

    private void clearFields() {
        stationNameEnglishInput.clear();
        stationNameMyanmarInput.clear();
        latitudeInput.clear();
        longitudeInput.clear();
        stationAddressEnglishInput.clear();
        stationAddressMyanmarInput.clear();
        clearFormWarnings();
    }

    @ActionMethod("createDenkoStation")
    public void createDenkoStation() {

        if (validateBeforeFormSubmission()) {

            DenkoStation denkoStation = new DenkoStation();
            loadFormDataIntoDenkoStation(denkoStation);
            List<DenkoStation> denkoStations = denkoDenkoStationRestConsumer.saveDenkoStation(denkoStation);
            refreshTable(denkoStations);
            currentDenkoStation = denkoStation;
            select(denkoStation, denkoStations);
        }

        clear();
    }

    @ActionMethod("updateDenkoStation")
    public void updateDenkoStation() {

        if (validateBeforeFormSubmission()) {

            DenkoStation denkoStationToUpdate = currentDenkoStation;
            loadFormDataIntoDenkoStation(denkoStationToUpdate);
            List<DenkoStation> denkoStations = denkoDenkoStationRestConsumer.updateDenkoStation(denkoStationToUpdate.getId(), denkoStationToUpdate);
            refreshTable(denkoStations);
            select(denkoStationToUpdate, denkoStations);
            loadDenkoStationDetails();
        }

        clear();
    }

    private void clearFormWarnings() {
        stationNameEnglishInput.setStyle("-fx-border-color: null");
        stationNameMyanmarInput.setStyle("-fx-border-color: null");
        latitudeInput.setStyle("-fx-border-color: null");
        longitudeInput.setStyle("-fx-border-color: null");
        stationAddressEnglishInput.setStyle("-fx-border-color: null");
        stationAddressMyanmarInput.setStyle("-fx-border-color: null");
    }

    private boolean validateBeforeFormSubmission() {
        boolean isErrorFree = true;

        if (stationNameEnglishInput.getText().isEmpty()) {
            stationNameEnglishInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }

        if (stationNameMyanmarInput.getText().isEmpty()) {
            stationNameMyanmarInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }

        if (latitudeInput.getText().isEmpty()) {
            latitudeInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }

        if (longitudeInput.getText().isEmpty()) {
            longitudeInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }

        if (stationAddressEnglishInput.getText().isEmpty()) {
            stationAddressEnglishInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }

        if (stationAddressEnglishInput.getText().isEmpty()) {
            stationAddressEnglishInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }

        return isErrorFree;
    }

    private void loadFormDataIntoDenkoStation(DenkoStation denkoStation) {
        denkoStation.setStationNameEnglish(stationNameEnglishInput.getText());
        denkoStation.setStationNameMyanmar(stationNameMyanmarInput.getText());
        denkoStation.setLatitude(Double.parseDouble(latitudeInput.getText()));
        denkoStation.setLongitude(Double.parseDouble(longitudeInput.getText()));
        denkoStation.setStationAddressEnglish(stationAddressEnglishInput.getText());
        denkoStation.setStationAddressMyanmar(stationAddressMyanmarInput.getText());
    }

    private void loadDenkoStationIntoForm(DenkoStation denkoStation) {
        stationNameEnglishInput.setText(denkoStation.getStationNameEnglish());
        stationNameMyanmarInput.setText(denkoStation.getStationNameMyanmar());
        latitudeInput.setText(Utilities.toCurrencyFormat(denkoStation.getLatitude()));
        longitudeInput.setText(Utilities.toCurrencyFormat(denkoStation.getLongitude()));
        stationAddressEnglishInput.setText(denkoStation.getStationAddressEnglish());
        stationAddressMyanmarInput.setText(denkoStation.getStationAddressMyanmar());
    }

    private void select(DenkoStation denkoStation, List<DenkoStation> denkoStations) {
        for (DenkoStation currentlyTraversedDenkoStation : denkoStations) {
            if (currentlyTraversedDenkoStation.getId() == denkoStation.getId()) {
                denkoStationTableView.getSelectionModel().select(currentlyTraversedDenkoStation);
                break;
            }
        }
    }

    @ActionMethod("reload")
    public void loadTable() {
        List<DenkoStation> denkoStations = denkoDenkoStationRestConsumer.getAllDenkoStations();
        denkoStationTableView.getItems().setAll(denkoStations);
    }

    private void refreshTable(List<DenkoStation> denkoStations) {
        denkoStationTableView.getItems().clear();
        denkoStationTableView.getItems().setAll(denkoStations);
    }

    @ActionMethod("loadEditDenkoStationForm")
    public void loadEditDenkoStationForm() {
        prepareEditDenkoStation();
        loadNewDenkoStationGrid();
        loadDenkoStationIntoForm(currentDenkoStation);
    }

    private void prepareEditDenkoStation() {
        newDenkoStationText.setText("Edit");
        createDenkoStationButton.setVisible(false);
        saveDenkoStationButton.setVisible(true);
        saveDenkoStationButton.toFront();
    }

    @ActionMethod("loadDeleteDenkoStationForm")
    public void loadDeleteDenkoStationForm() {
        if (checkBeforeDeleting())
            loadDeleteDenkoStationGrid();
        else
            loadUndeletableDenkoStationGrid();
    }

    private void loadDeleteDenkoStationGrid() {
        enableThisGrid(confirmDeleteDenkoStationGrid);
    }

    private void loadUndeletableDenkoStationGrid() {
        enableThisGrid(undeletableDenkoStationGrid);
    }

    @ActionMethod("confirmDeleteDenkoStation")
    public void confirmDeleteDenkoStation() {
        List<DenkoStation> denkoStations = denkoDenkoStationRestConsumer.deleteDenkoStation(currentDenkoStation.getId());
        refreshTable(denkoStations);
        loadNewDenkoStationForm();
    }

    @ActionMethod("cancelDeleteDenkoStation")
    public void cancelDeleteDenkoStation() {
        loadDenkoStationDetails();
    }

    @ActionMethod("okUndeletableDenkoStation")
    public void okUndeletableDenkoStation() {
        loadDenkoStationDetails();
    }

    private boolean isDeletable() {
        return true;
    }

    private boolean checkBeforeDeleting() {
        return true;
    }
}
