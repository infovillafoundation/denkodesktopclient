package org.bitbucket.infovillafoundation.denko.mobile.controllers;

import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.BackAction;
import io.datafx.controller.flow.action.LinkAction;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

import javax.annotation.PostConstruct;

/**
 * Created by Sandah Aung on 4/3/15.
 */

@FXMLController(value = "/fxml/menu.fxml", title = "Admin Menu")
public class MenuController {

    @FXML
    @BackAction
    private Button backButton;

    @FXML
    @LinkAction(MessageController.class)
    private Button messageButton;

    @FXML
    @LinkAction(PriceController.class)
    private Button priceButton;

    @FXML
    @LinkAction(DenkoStationController.class)
    private Button stationButton;

    @PostConstruct
    public void init() {
        messageButton.setDisable(false);
        priceButton.setDisable(false);
        stationButton.setDisable(false);
    }
}
