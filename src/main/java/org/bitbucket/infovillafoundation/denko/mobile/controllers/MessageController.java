package org.bitbucket.infovillafoundation.denko.mobile.controllers;

import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.action.BackAction;
import io.datafx.controller.flow.context.ActionHandler;
import io.datafx.controller.flow.context.FlowActionHandler;
import io.datafx.controller.util.VetoException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.util.Callback;
import org.bitbucket.infovillafoundation.denko.mobile.cells.MessageDateTableCell;
import org.bitbucket.infovillafoundation.denko.mobile.entities.Message;
import org.bitbucket.infovillafoundation.denko.mobile.restservice.consumers.DenkoMessageRestConsumer;
import org.bitbucket.infovillafoundation.denko.mobile.utils.Utilities;
import org.controlsfx.control.MasterDetailPane;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 16/12/14.
 */

@FXMLController(value = "/fxml/messages.fxml", title = "Messages")
public class MessageController {

    @ActionHandler
    private FlowActionHandler handler;

    @FXML
    private MasterDetailPane pane;

    @FXML
    private GridPane addMessageGrid;

    @FXML
    private GridPane showMessageGrid;

    @FXML
    private GridPane confirmDeleteMessageGrid;

    @FXML
    private GridPane undeletableMessageGrid;

    @FXML
    @BackAction
    private Button backButton;

    @FXML
    @ActionTrigger("reload")
    private Button reloadButton;

    @FXML
    @ActionTrigger("loadNewMessageForm")
    private Button newMessageButton;

    @FXML
    @ActionTrigger("loadEditMessageForm")
    private Button editMessageButton;

    @FXML
    @ActionTrigger("loadDeleteMessageForm")
    private Button deleteMessageButton;

    @FXML
    private TableView<Message> messageTableView;

    @FXML
    private TableColumn<Message, Date> messageDateColumn;

    @FXML
    @ActionTrigger("loadMessageDetails")
    private MenuItem detailsMenuItem;

    @FXML
    private Text newMessageText;

    @FXML
    private TextArea englishMessageInput;

    @FXML
    private TextArea myanmarMessageInput;

    @FXML
    @ActionTrigger("createMessage")
    private Button createMessageButton;

    @FXML
    @ActionTrigger("updateMessage")
    private Button saveMessageButton;

    @FXML
    @ActionTrigger("clearMessage")
    private Button clearMessageButton;

    @FXML
    private Text englishMessageText;

    @FXML
    private Text myanmarMessageText;

    @FXML
    private Text messageDateText;

    @FXML
    @ActionTrigger("confirmDeleteMessage")
    private Button confirmDeleteMessageButton;

    @FXML
    @ActionTrigger("cancelDeleteMessage")
    private Button cancelDeleteMessageButton;

    @FXML
    @ActionTrigger("okUndeletableMessage")
    private Button okUndeletableMessageButton;

    @FXML
    private HBox saveEditMessageButtonBar;

    @Inject
    private DenkoMessageRestConsumer denkoMessageRestConsumer;

    private Message currentMessage;

    @PostConstruct
    public void init() {
        setupMessageDateColumn();
        loadTable();
        setupBackShortcutListener();
        setupRowClickListener();
        loadNewMessageForm();
        setupValidators();
    }

    private void setupMessageDateColumn() {
        messageDateColumn.setCellFactory(new Callback<TableColumn<Message, Date>, TableCell<Message, Date>>() {
            @Override
            public TableCell<Message, Date> call(TableColumn<Message, Date> param) {
                return new MessageDateTableCell();
            }
        });
    }

    private void setupRowClickListener() {
        messageTableView
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(new ChangeListener<Message>() {
                                 @Override
                                 public void changed(ObservableValue<? extends Message> observable, Message oldValue, Message newValue) {
                                     currentMessage = newValue;
                                     if (currentMessage == null) {
                                         loadNewMessageForm();
                                         editMessageButton.setVisible(false);
                                         deleteMessageButton.setVisible(false);
                                     } else {
                                         loadMessageDetails();
                                         editMessageButton.setVisible(true);
                                         deleteMessageButton.setVisible(true);
                                     }

                                 }
                             }

                );
    }

    private void setupValidators() {
        setupTextAreaValidator(englishMessageInput);
        setupTextAreaValidator(myanmarMessageInput);
    }

    private void setupTextAreaValidator(TextArea textArea) {
        textArea.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (textArea.getText().isEmpty())
                    textArea.setStyle("-fx-border-color: red");
                else
                    textArea.setStyle("-fx-border-color: null");
            }
        });
    }

    private void setupBackShortcutListener() {
        final KeyCombination ctrlB = new KeyCodeCombination(KeyCode.B, KeyCombination.CONTROL_DOWN);
        pane.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (ctrlB.match(event)) {
                    try {
                        handler.navigateBack();
                    } catch (VetoException e) {
                    } catch (FlowException e) {
                    }
                }
            }
        });
    }

    @ActionMethod("loadMessageDetails")
    public void loadMessageDetails() {
        loadMessageDetailsGrid();
        loadMessageIntoShowGrid();
    }

    private void loadMessageDetailsGrid() {
        enableThisGrid(showMessageGrid);
    }

    private void enableThisGrid(GridPane grid) {
        addMessageGrid.setVisible(false);
        showMessageGrid.setVisible(false);
        confirmDeleteMessageGrid.setVisible(false);
        undeletableMessageGrid.setVisible(false);
        grid.setVisible(true);
        grid.toFront();
    }

    private void loadMessageIntoShowGrid() {
        englishMessageText.setText(currentMessage.getEnglishMessage());
        myanmarMessageText.setText(currentMessage.getMyanmarMessage());
        messageDateText.setText(Utilities.toDateString(currentMessage.getMessageDate()));
    }

    @ActionMethod("loadNewMessageForm")
    public void loadNewMessageForm() {
        loadNewMessageGrid();
        prepareNewMessage();
    }

    private void prepareNewMessage() {
        saveMessageButton.setVisible(false);
        createMessageButton.setVisible(true);
        createMessageButton.toFront();
    }

    private void loadNewMessageGrid() {
        enableThisGrid(addMessageGrid);
        clearFields();
    }

    @ActionMethod("clearMessage")
    public void clear() {
        clearFields();
    }

    private void clearFields() {
        englishMessageInput.clear();
        myanmarMessageInput.clear();
        clearFormWarnings();
    }

    @ActionMethod("createMessage")
    public void createMessage() {

        if (validateBeforeFormSubmission()) {

            Message message = new Message();
            loadFormDataIntoMessage(message);
            List<Message> messages = denkoMessageRestConsumer.saveMessage(message);
            refreshTable(messages);
            currentMessage = message;
            select(message, messages);
        }

        clear();
    }

    @ActionMethod("updateMessage")
    public void updateMessage() {

        if (validateBeforeFormSubmission()) {

            Message messageToUpdate = currentMessage;
            loadFormDataIntoMessage(messageToUpdate);
            List<Message> messages = denkoMessageRestConsumer.updateMessage(messageToUpdate.getId(), messageToUpdate);
            refreshTable(messages);
            select(messageToUpdate, messages);
            loadMessageDetails();
        }

        clear();
    }

    private void clearFormWarnings() {
        englishMessageInput.setStyle("-fx-border-color: null");
        myanmarMessageInput.setStyle("-fx-border-color: null");
    }

    private boolean validateBeforeFormSubmission() {
        boolean isErrorFree = true;

        if (englishMessageInput.getText().isEmpty()) {
            englishMessageInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }

        if (myanmarMessageInput.getText().isEmpty()) {
            myanmarMessageInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }

        return isErrorFree;
    }

    private void loadFormDataIntoMessage(Message message) {
        message.setEnglishMessage(englishMessageInput.getText());
        message.setMyanmarMessage(myanmarMessageInput.getText());
    }

    private void loadMessageIntoForm(Message message) {
        englishMessageInput.setText(message.getEnglishMessage());
        myanmarMessageInput.setText(message.getMyanmarMessage());
    }

    private void select(Message message, List<Message> messages) {
        for (Message currentlyTraversedMessage : messages) {
            if (currentlyTraversedMessage.getId() == message.getId()) {
                messageTableView.getSelectionModel().select(currentlyTraversedMessage);
                break;
            }
        }
    }

    @ActionMethod("reload")
    public void loadTable() {
        List<Message> messages = denkoMessageRestConsumer.getAllMessages();
        messageTableView.getItems().setAll(messages);
    }

    private void refreshTable(List<Message> messages) {
        messageTableView.getItems().clear();
        messageTableView.getItems().setAll(messages);
    }

    @ActionMethod("loadEditMessageForm")
    public void loadEditMessageForm() {
        prepareEditMessage();
        loadNewMessageGrid();
        loadMessageIntoForm(currentMessage);
    }

    private void prepareEditMessage() {
        newMessageText.setText("Edit");
        createMessageButton.setVisible(false);
        saveMessageButton.setVisible(true);
        saveMessageButton.toFront();
    }

    @ActionMethod("loadDeleteMessageForm")
    public void loadDeleteMessageForm() {
        if (checkBeforeDeleting())
            loadDeleteMessageGrid();
        else
            loadUndeletableMessageGrid();
    }

    private void loadDeleteMessageGrid() {
        enableThisGrid(confirmDeleteMessageGrid);
    }

    private void loadUndeletableMessageGrid() {
        enableThisGrid(undeletableMessageGrid);
    }

    @ActionMethod("confirmDeleteMessage")
    public void confirmDeleteMessage() {
        List<Message> messages = denkoMessageRestConsumer.deleteMessage(currentMessage.getId());
        refreshTable(messages);
        loadNewMessageForm();
    }

    @ActionMethod("cancelDeleteMessage")
    public void cancelDeleteMessage() {
        loadMessageDetails();
    }

    @ActionMethod("okUndeletableMessage")
    public void okUndeletableMessage() {
        loadMessageDetails();
    }

    private boolean isDeletable() {
        return true;
    }

    private boolean checkBeforeDeleting() {
        return true;
    }
}
