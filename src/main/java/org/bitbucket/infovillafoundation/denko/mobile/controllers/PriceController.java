package org.bitbucket.infovillafoundation.denko.mobile.controllers;

import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.action.BackAction;
import io.datafx.controller.flow.context.ActionHandler;
import io.datafx.controller.flow.context.FlowActionHandler;
import io.datafx.controller.util.VetoException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.util.Callback;
import org.bitbucket.infovillafoundation.denko.mobile.cells.PricePostDateTableCell;
import org.bitbucket.infovillafoundation.denko.mobile.entities.Price;
import org.bitbucket.infovillafoundation.denko.mobile.restservice.consumers.DenkoPriceRestConsumer;
import org.bitbucket.infovillafoundation.denko.mobile.utils.Utilities;
import org.controlsfx.control.MasterDetailPane;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 16/12/14.
 */

@FXMLController(value = "/fxml/prices.fxml", title = "Prices")
public class PriceController {

    @ActionHandler
    private FlowActionHandler handler;

    @FXML
    private MasterDetailPane pane;

    @FXML
    private GridPane addPriceGrid;

    @FXML
    private GridPane showPriceGrid;

    @FXML
    private GridPane confirmDeletePriceGrid;

    @FXML
    private GridPane undeletablePriceGrid;

    @FXML
    @BackAction
    private Button backButton;

    @FXML
    @ActionTrigger("reload")
    private Button reloadButton;

    @FXML
    @ActionTrigger("loadNewPriceForm")
    private Button newPriceButton;

    @FXML
    @ActionTrigger("loadEditPriceForm")
    private Button editPriceButton;

    @FXML
    @ActionTrigger("loadDeletePriceForm")
    private Button deletePriceButton;

    @FXML
    private TableView<Price> priceTableView;

    @FXML
    private TableColumn<Price, Date> postDateColumn;

    @FXML
    @ActionTrigger("loadPriceDetails")
    private MenuItem detailsMenuItem;

    @FXML
    private Text newPriceText;

    @FXML
    private TextField ron95PriceInput;

    @FXML
    private TextField ron92PriceInput;

    @FXML
    private TextField specialDieselPriceInput;

    @FXML
    private TextField normalDieselPriceInput;

    @FXML
    @ActionTrigger("createPrice")
    private Button createPriceButton;

    @FXML
    @ActionTrigger("updatePrice")
    private Button savePriceButton;

    @FXML
    @ActionTrigger("clearPrice")
    private Button clearPriceButton;

    @FXML
    private Text ron95PriceText;

    @FXML
    private Text ron92PriceText;

    @FXML
    private Text specialDieselPriceText;

    @FXML
    private Text normalDieselPriceText;

    @FXML
    private Text postDateText;

    @FXML
    @ActionTrigger("confirmDeletePrice")
    private Button confirmDeletePriceButton;

    @FXML
    @ActionTrigger("cancelDeletePrice")
    private Button cancelDeletePriceButton;

    @FXML
    @ActionTrigger("okUndeletablePrice")
    private Button okUndeletablePriceButton;

    @FXML
    private HBox saveEditPriceButtonBar;

    @Inject
    private DenkoPriceRestConsumer denkoPriceRestConsumer;

    private Price currentPrice;

    @PostConstruct
    public void init() {
        setupPriceDateColumn();
        loadTable();
        setupBackShortcutListener();
        setupRowClickListener();
        loadNewPriceForm();
        setupValidators();
    }

    private void setupPriceDateColumn() {
        postDateColumn.setCellFactory(new Callback<TableColumn<Price, Date>, TableCell<Price, Date>>() {
            @Override
            public TableCell<Price, Date> call(TableColumn<Price, Date> param) {
                return new PricePostDateTableCell();
            }
        });
    }

    private void setupRowClickListener() {
        priceTableView
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(new ChangeListener<Price>() {
                                 @Override
                                 public void changed(ObservableValue<? extends Price> observable, Price oldValue, Price newValue) {
                                     currentPrice = newValue;
                                     if (currentPrice == null) {
                                         loadNewPriceForm();
                                         editPriceButton.setVisible(false);
                                         deletePriceButton.setVisible(false);
                                     } else {
                                         loadPriceDetails();
                                         editPriceButton.setVisible(true);
                                         deletePriceButton.setVisible(true);
                                     }

                                 }
                             }

                );
    }

    private void setupValidators() {
        setupTextFieldValidator(ron95PriceInput);
        setupTextFieldValidator(ron92PriceInput);
        setupTextFieldValidator(specialDieselPriceInput);
        setupTextFieldValidator(normalDieselPriceInput);
    }

    private void setupTextFieldValidator(TextField textField) {
        textField.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (textField.getText().isEmpty())
                    textField.setStyle("-fx-border-color: red");
                else
                    textField.setStyle("-fx-border-color: null");
            }
        });
    }

    private void setupBackShortcutListener() {
        final KeyCombination ctrlB = new KeyCodeCombination(KeyCode.B, KeyCombination.CONTROL_DOWN);
        pane.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (ctrlB.match(event)) {
                    try {
                        handler.navigateBack();
                    } catch (VetoException e) {
                    } catch (FlowException e) {
                    }
                }
            }
        });
    }

    @ActionMethod("loadPriceDetails")
    public void loadPriceDetails() {
        loadPriceDetailsGrid();
        loadPriceIntoShowGrid();
    }

    private void loadPriceDetailsGrid() {
        enableThisGrid(showPriceGrid);
    }

    private void enableThisGrid(GridPane grid) {
        addPriceGrid.setVisible(false);
        showPriceGrid.setVisible(false);
        confirmDeletePriceGrid.setVisible(false);
        undeletablePriceGrid.setVisible(false);
        grid.setVisible(true);
        grid.toFront();
    }

    private void loadPriceIntoShowGrid() {
        ron95PriceText.setText(Utilities.toCurrencyFormat(currentPrice.getRon95()));
        ron92PriceText.setText(Utilities.toCurrencyFormat(currentPrice.getRon92()));
        normalDieselPriceText.setText(Utilities.toCurrencyFormat(currentPrice.getDieselNormal()));
        specialDieselPriceText.setText(Utilities.toCurrencyFormat(currentPrice.getDieselSpecial()));
        postDateText.setText(Utilities.toDateString(currentPrice.getPostDate()));
    }

    @ActionMethod("loadNewPriceForm")
    public void loadNewPriceForm() {
        loadNewPriceGrid();
        prepareNewPrice();
    }

    private void prepareNewPrice() {
        savePriceButton.setVisible(false);
        createPriceButton.setVisible(true);
        createPriceButton.toFront();
    }

    private void loadNewPriceGrid() {
        enableThisGrid(addPriceGrid);
        clearFields();
    }

    @ActionMethod("clearPrice")
    public void clear() {
        clearFields();
    }

    private void clearFields() {
        ron95PriceInput.clear();
        ron92PriceInput.clear();
        specialDieselPriceInput.clear();
        normalDieselPriceInput.clear();
        clearFormWarnings();
    }

    @ActionMethod("createPrice")
    public void createPrice() {

        if (validateBeforeFormSubmission()) {

            Price price = new Price();
            loadFormDataIntoPrice(price);
            List<Price> prices = denkoPriceRestConsumer.savePrice(price);
            refreshTable(prices);
            currentPrice = price;
            select(price, prices);
        }
    }

    @ActionMethod("updatePrice")
    public void updatePrice() {

        if (validateBeforeFormSubmission()) {

            Price priceToUpdate = currentPrice;
            loadFormDataIntoPrice(priceToUpdate);
            List<Price> prices = denkoPriceRestConsumer.updatePrice(priceToUpdate.getId(), priceToUpdate);
            refreshTable(prices);
            select(priceToUpdate, prices);
            loadPriceDetails();
        }
    }

    private void clearFormWarnings() {
        ron95PriceInput.setStyle("-fx-border-color: null");
        ron92PriceInput.setStyle("-fx-border-color: null");
        specialDieselPriceInput.setStyle("-fx-border-color: null");
        normalDieselPriceInput.setStyle("-fx-border-color: null");
    }

    private boolean validateBeforeFormSubmission() {
        boolean isErrorFree = true;

        if (ron92PriceInput.getText().isEmpty()) {
            ron92PriceInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }

        if (ron95PriceInput.getText().isEmpty()) {
            ron95PriceInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }

        if (specialDieselPriceInput.getText().isEmpty()) {
            specialDieselPriceInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }

        if (normalDieselPriceInput.getText().isEmpty()) {
            normalDieselPriceInput.setStyle("-fx-border-color: red");
            isErrorFree = false;
        }

        return isErrorFree;
    }

    private void loadFormDataIntoPrice(Price price) {
        price.setRon95(Double.parseDouble(ron95PriceInput.getText()));
        price.setRon92(Double.parseDouble(ron92PriceInput.getText()));
        price.setDieselSpecial(Double.parseDouble(specialDieselPriceInput.getText()));
        price.setDieselNormal(Double.parseDouble(normalDieselPriceInput.getText()));
    }

    private void loadPriceIntoForm(Price price) {
        ron95PriceInput.setText(Utilities.toCurrencyFormat(price.getRon95()));
        ron92PriceInput.setText(Utilities.toCurrencyFormat(price.getRon92()));
        specialDieselPriceInput.setText(Utilities.toCurrencyFormat(price.getDieselSpecial()));
        normalDieselPriceInput.setText(Utilities.toCurrencyFormat(price.getDieselNormal()));
    }

    private void select(Price price, List<Price> prices) {
        for (Price currentlyTraversedPrice : prices) {
            if (currentlyTraversedPrice.getId() == price.getId()) {
                priceTableView.getSelectionModel().select(currentlyTraversedPrice);
                break;
            }
        }
    }

    @ActionMethod("reload")
    public void loadTable() {
        List<Price> prices = denkoPriceRestConsumer.getAllPrices();
        priceTableView.getItems().setAll(prices);
    }

    private void refreshTable(List<Price> prices) {
        priceTableView.getItems().clear();
        priceTableView.getItems().setAll(prices);
    }

    @ActionMethod("loadEditPriceForm")
    public void loadEditPriceForm() {
        prepareEditPrice();
        loadNewPriceGrid();
        loadPriceIntoForm(currentPrice);
    }

    private void prepareEditPrice() {
        newPriceText.setText("Edit");
        createPriceButton.setVisible(false);
        savePriceButton.setVisible(true);
        savePriceButton.toFront();
    }

    @ActionMethod("loadDeletePriceForm")
    public void loadDeletePriceForm() {
        if (checkBeforeDeleting())
            loadDeletePriceGrid();
        else
            loadUndeletablePriceGrid();
    }

    private void loadDeletePriceGrid() {
        enableThisGrid(confirmDeletePriceGrid);
    }

    private void loadUndeletablePriceGrid() {
        enableThisGrid(undeletablePriceGrid);
    }

    @ActionMethod("confirmDeletePrice")
    public void confirmDeletePrice() {
        List<Price> prices = denkoPriceRestConsumer.deletePrice(currentPrice.getId());
        refreshTable(prices);
        loadNewPriceForm();
    }

    @ActionMethod("cancelDeletePrice")
    public void cancelDeletePrice() {
        loadPriceDetails();
    }

    @ActionMethod("okUndeletablePrice")
    public void okUndeletableMessage() {
        loadPriceDetails();
    }

    private boolean isDeletable() {
        return true;
    }

    private boolean checkBeforeDeleting() {
        return true;
    }
}
