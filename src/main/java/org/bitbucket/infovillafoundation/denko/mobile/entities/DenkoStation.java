package org.bitbucket.infovillafoundation.denko.mobile.entities;

/**
 * Created by Sandah Aung on 29/3/15.
 */

public class DenkoStation {

    private Long id;

    private String stationNameEnglish;

    private String stationNameMyanmar;

    private Double latitude;

    private Double longitude;

    private String stationAddressEnglish;

    private String stationAddressMyanmar;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStationNameEnglish() {
        return stationNameEnglish;
    }

    public void setStationNameEnglish(String stationNameEnglish) {
        this.stationNameEnglish = stationNameEnglish;
    }

    public String getStationNameMyanmar() {
        return stationNameMyanmar;
    }

    public void setStationNameMyanmar(String stationNameMyanmar) {
        this.stationNameMyanmar = stationNameMyanmar;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getStationAddressEnglish() {
        return stationAddressEnglish;
    }

    public void setStationAddressEnglish(String stationAddressEnglish) {
        this.stationAddressEnglish = stationAddressEnglish;
    }

    public String getStationAddressMyanmar() {
        return stationAddressMyanmar;
    }

    public void setStationAddressMyanmar(String stationAddressMyanmar) {
        this.stationAddressMyanmar = stationAddressMyanmar;
    }
}
