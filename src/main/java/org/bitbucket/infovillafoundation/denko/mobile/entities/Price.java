package org.bitbucket.infovillafoundation.denko.mobile.entities;

import java.util.Date;

/**
 * Created by Sandah Aung on 29/3/15.
 */

public class Price {

    private long id;

    private Double ron95;

    private Double ron92;

    private Double dieselNormal;

    private Double dieselSpecial;

    private Date postDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Double getRon95() {
        return ron95;
    }

    public void setRon95(Double ron95) {
        this.ron95 = ron95;
    }

    public Double getRon92() {
        return ron92;
    }

    public void setRon92(Double ron92) {
        this.ron92 = ron92;
    }

    public Double getDieselNormal() {
        return dieselNormal;
    }

    public void setDieselNormal(Double dieselNormal) {
        this.dieselNormal = dieselNormal;
    }

    public Double getDieselSpecial() {
        return dieselSpecial;
    }

    public void setDieselSpecial(Double dieselSpecial) {
        this.dieselSpecial = dieselSpecial;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }
}
