package org.bitbucket.infovillafoundation.denko.mobile.fxcomponents;

import javafx.scene.control.TextField;

import java.util.regex.Pattern;

/**
 * Created by Sandah Aung on 9/3/15.
 */

public class MoneyTextField extends TextField {

    final Pattern pattern = Pattern.compile("^\\d*\\.?\\d{0,2}");

    @Override
    public void replaceText(int start, int end, String text) {
        String newText = getText().substring(0, start) + text + getText().substring(end);
        if (pattern.matcher(newText).matches()) {
            super.replaceText(start, end, text);
        }
    }

    @Override
    public void replaceSelection(String text) {
        int start = getSelection().getStart();
        int end = getSelection().getEnd();
        String newText = getText().substring(0, start) + text + getText().substring(end);
        if (pattern.matcher(newText).matches()) {
            super.replaceSelection(text);
        }
    }
}