package org.bitbucket.infovillafoundation.denko.model;

import org.bitbucket.infovillafoundation.denko.mobile.entities.DenkoStation;
import org.bitbucket.infovillafoundation.denko.mobile.entities.Message;

import java.util.List;

/**
 * Created by Sandah Aung on 31/3/15.
 */

public class DenkoModel {

    private List<Message> messages;

    private List<DenkoStation> denkoStations;

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public List<DenkoStation> getDenkoStations() {
        return denkoStations;
    }

    public void setDenkoStations(List<DenkoStation> denkoStations) {
        this.denkoStations = denkoStations;
    }
}
