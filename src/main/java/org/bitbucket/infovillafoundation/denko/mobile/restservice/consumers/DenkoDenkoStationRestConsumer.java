package org.bitbucket.infovillafoundation.denko.mobile.restservice.consumers;

import io.datafx.controller.injection.scopes.ApplicationScoped;
import org.bitbucket.infovillafoundation.denko.mobile.entities.DenkoStation;
import org.bitbucket.infovillafoundation.denko.mobile.restservice.interfaces.DenkoDenkoStationRestService;
import org.bitbucket.infovillafoundation.denko.mobile.restservice.resttarget.RestTarget;

import javax.ws.rs.PathParam;
import java.util.List;

/**
 * Created by Sandah Aung on 25/3/15.
 */

@ApplicationScoped
public class DenkoDenkoStationRestConsumer implements DenkoDenkoStationRestService {

    private DenkoDenkoStationRestService service;


    public DenkoDenkoStationRestConsumer() {
        if (service == null)
            service = RestTarget.getRestTarget().proxy(DenkoDenkoStationRestService.class);
    }

    public List<DenkoStation> getAllDenkoStations() {
        return service.getAllDenkoStations();
    }

    public DenkoStation getDenkoStationById(long id) {
        return service.getDenkoStationById(id);
    }

    public List<DenkoStation> saveDenkoStation(DenkoStation denkoStation) {
        return service.saveDenkoStation(denkoStation);
    }

    public List<DenkoStation> deleteDenkoStation(long id) {
        return service.deleteDenkoStation(id);
    }

    public List<DenkoStation> updateDenkoStation(long id, DenkoStation denkoStation) {
        return service.updateDenkoStation(id, denkoStation);
    }

    @Override
    public boolean isDeletable(@PathParam("id") long id) {
        return true;
    }

}
