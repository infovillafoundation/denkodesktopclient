package org.bitbucket.infovillafoundation.denko.mobile.restservice.consumers;

import io.datafx.controller.injection.scopes.ApplicationScoped;
import org.bitbucket.infovillafoundation.denko.mobile.entities.Message;
import org.bitbucket.infovillafoundation.denko.mobile.restservice.interfaces.DenkoMessageRestService;
import org.bitbucket.infovillafoundation.denko.mobile.restservice.resttarget.RestTarget;

import javax.ws.rs.PathParam;
import java.util.List;

/**
 * Created by Sandah Aung on 25/3/15.
 */

@ApplicationScoped
public class DenkoMessageRestConsumer implements DenkoMessageRestService {

    private DenkoMessageRestService service;


    public DenkoMessageRestConsumer() {
        if (service == null)
            service = RestTarget.getRestTarget().proxy(DenkoMessageRestService.class);
    }

    public List<Message> getAllMessages() {
        return service.getAllMessages();
    }

    public Message getMessageById(long id) {
        return service.getMessageById(id);
    }

    public List<Message> saveMessage(Message message) {
        return service.saveMessage(message);
    }

    public List<Message> deleteMessage(long id) {
        return service.deleteMessage(id);
    }

    public List<Message> updateMessage(long id, Message message) {
        return service.updateMessage(id, message);
    }

    @Override
    public boolean isDeletable(@PathParam("id") long id) {
        return true;
    }

}
