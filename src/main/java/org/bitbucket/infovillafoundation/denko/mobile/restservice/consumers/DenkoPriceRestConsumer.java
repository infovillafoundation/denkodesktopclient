package org.bitbucket.infovillafoundation.denko.mobile.restservice.consumers;

import io.datafx.controller.injection.scopes.ApplicationScoped;
import org.bitbucket.infovillafoundation.denko.mobile.entities.Price;
import org.bitbucket.infovillafoundation.denko.mobile.restservice.interfaces.DenkoPriceRestService;
import org.bitbucket.infovillafoundation.denko.mobile.restservice.resttarget.RestTarget;

import javax.ws.rs.PathParam;
import java.util.List;

/**
 * Created by Sandah Aung on 25/3/15.
 */

@ApplicationScoped
public class DenkoPriceRestConsumer implements DenkoPriceRestService {

    private DenkoPriceRestService service;


    public DenkoPriceRestConsumer() {
        if (service == null)
            service = RestTarget.getRestTarget().proxy(DenkoPriceRestService.class);
    }

    public List<Price> getAllPrices() {
        return service.getAllPrices();
    }

    public Price getPriceById(long id) {
        return service.getPriceById(id);
    }

    public List<Price> savePrice(Price price) {
        return service.savePrice(price);
    }

    public List<Price> deletePrice(long id) {
        return service.deletePrice(id);
    }

    public List<Price> updatePrice(long id, Price price) {
        return service.updatePrice(id, price);
    }

    @Override
    public boolean isDeletable(@PathParam("id") long id) {
        return true;
    }

}
