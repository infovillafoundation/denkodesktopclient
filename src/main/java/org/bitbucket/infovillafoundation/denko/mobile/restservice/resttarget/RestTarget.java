package org.bitbucket.infovillafoundation.denko.mobile.restservice.resttarget;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

/**
 * Created by Sandah Aung on 5/3/15.
 */
public class RestTarget {

    private static ResteasyWebTarget target;

    public static ResteasyWebTarget getRestTarget() {
        if (target == null) {
            ResteasyClient client = new ResteasyClientBuilder().build();
            target = client.target("http://denko.jelastic.skali.net/");
        }
        return target;
    }
}
