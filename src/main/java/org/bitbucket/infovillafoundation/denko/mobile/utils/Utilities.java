package org.bitbucket.infovillafoundation.denko.mobile.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Sandah Aung on 10/3/15.
 */

public class Utilities {

    private static SimpleDateFormat simpleDateFormat;

    public static String toCurrencyFormat(double amount) {
        return String.format("%.2f", amount);
    }

    public static String toDateString(Date date) {
        if (simpleDateFormat == null)
            simpleDateFormat = new SimpleDateFormat("dd/M/yyyy HH:mm:ss");

        return simpleDateFormat.format(date);
    }
}
